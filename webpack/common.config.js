const path = require('path');
const autoprefixer = require('autoprefixer');
const postcssImport = require('postcss-import');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');


const development = require('./dev.config.js');
const production = require('./prod.config.js');

const TARGET = process.env.npm_lifecycle_event;

const PATHS = {
    app: path.join(__dirname, '../src/app'),
    build: path.join(__dirname, '../dist')
};

const VENDOR = [
    'history',
    'babel-polyfill',
    'react',
    'react-dom',
    'react-redux',
    'react-router',
    'react-mixin',
    'classnames',
    'redux',
    'react-router-redux'
];

process.env.BABEL_ENV = TARGET;

const common = {
    entry: {
        app: PATHS.app,
        vendor: VENDOR
    },

    output: {
        filename: '[name].[hash].js',
        path: PATHS.build,
        chunkFilename: '[name]-[hash].js'
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, '../src/index.html'),
            hash: true,
            filename: 'index.html',
            inject: 'body'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            children: true,
            async: true
        })
    ],

    resolve: {
        extensions: [
            '',
            '.jsx',
            '.js',
            '.json',
            '.scss'
        ],
        modules: [
            'node_modules'
        ]
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$/,
                loader: 'file',
                query: {
                    name: '/images/[name].[ext]?[hash]'
                }
            },
            {
                test: /\.woff(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    name: '/fonts/[name].[ext]',
                    limit: 10000,
                    mimetype: 'application/font-woff'
                }
            },
            {
                test: /\.woff2(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    name: '/fonts/[name].[ext]',
                    limit: 10000,
                    mimetype: 'application/font-woff2'
                }
            },
            {
                test: /\.ttf(\?.*)?$/,
                loader: 'file-loader',
                query: {
                    name: '/fonts/[name].[ext]'
                }
            },
            {
                test: /\.eot(\?.*)?$/,
                loader: 'file-loader',
                query: {
                    name: '/fonts/[name].[ext]'
                }
            },
            {
                test: /\.otf(\?.*)?$/,
                loader: 'file-loader',
                query: {
                    name: '/fonts/[name].[ext]',
                    mimetype: 'application/font-otf'
                }
            },
            {
                test: /\.svg(\?.*)?$/,
                loader: 'url-loader',
                query: {
                    name: '/fonts/[name].[ext]',
                    limit: 10000,
                    mimetype: 'image/svg+xml'
                }
            },
            {
                test: /\.json(\?.*)?$/,
                loader: 'file-loader',
                query: {
                    name: '/files/[name].[ext]'
                }
            }
        ]
    },

    sassLoader: {
        data: '@import "' + __dirname + '/../src/app/styles/config/_variables.scss";'
    },

    postcss: (param) => {
        return [
            autoprefixer({
                browsers: ['last 2 versions']
            }),
            postcssImport({
                addDependencyTo: param
            })
        ];
    }
};

if (TARGET === 'dev' || !TARGET) {
    module.exports = merge(development, common);
}

if (TARGET === 'prod' || !TARGET) {
    module.exports = merge(production, common);
}
