import { expect } from 'chai';
import nock from 'nock';
// import configureStore from 'redux-mock-store';
// import thunk from 'redux-thunk';

import * as TYPES from '../../src/app/constants';
import * as WEATHER_ACTIONS from '../../src/app/actions/weather';


describe('Weather Actions', () => {
    afterEach(() => {
        nock.cleanAll();
    });

    it('weatherRequest should create WEATHER_REQUEST action', () => {
        expect(WEATHER_ACTIONS.weatherRequest()).to.eql({
            type: TYPES.WEATHER_REQUEST
        });
    });

    // TODO: Mock requests
});
