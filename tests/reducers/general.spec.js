import { expect } from 'chai';
import reducer from '../../src/app/reducers/weather';

describe('General Reducers Tests', () => {
    it('the state should be the same when a action doesnt exist', () => {
        const reducerResponse = reducer(
            {
                data: 'some data',
                isFetching: false
            },
            {
                type: 'this action doesnt exist'
            }
        );
        expect(reducerResponse).to.eql({
            data: 'some data',
            isFetching: false
        });
    });

    it('the state should be the initial state when no state is present', () => {
        const initialState = {
            status: null,
            isFetching: false,
            data: null,
            errors: null
        };
        const reducerResponse = reducer(undefined,
            {
                type: 'this action doesnt exist'
            }
        );
        expect(reducerResponse).to.eql(initialState);
    });
});
