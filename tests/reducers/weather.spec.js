import { expect } from 'chai';
import weatherReducer from '../../src/app/reducers/weather';
import * as TYPES from '../../src/app/constants';

describe('Weather Reducers Tests', () => {
    it('should handle WEATHER_REQUEST', () => {
        const reducerResponse = weatherReducer([],
            {
                type: TYPES.WEATHER_REQUEST
            }
        );
        expect(reducerResponse).to.eql({
            isFetching: true
        });
    });

    it('should handle WEATHER_SUCCESS', () => {
        const reducerResponse = weatherReducer([],
            {
                type: TYPES.WEATHER_SUCCESS,
                payload: {
                    status: 200,
                    data: 'some data'
                }
            }
        );
        expect(reducerResponse).to.eql({
            isFetching: false,
            status: 200,
            data: 'some data',
            errors: null
        });
    });

    it('should handle WEATHER_FAILURE', () => {
        const reducerResponse = weatherReducer([],
            {
                type: TYPES.WEATHER_FAILURE,
                payload: {
                    status: 400,
                    errors: 'errors'
                }
            }
        );
        expect(reducerResponse).to.eql({
            isFetching: false,
            status: 400,
            data: null,
            errors: 'errors'
        });
    });
});
