import React from 'react';
import nock from 'nock';
import { expect } from 'chai';
import { mount, shallow } from 'enzyme';
import * as TYPES from '../../src/app/constants';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moment from 'moment';
import { default as HomeViewConnected, HomeViewNotConnected } from '../../src/app/containers/Home';

describe('Home View Tests (Container)', () => {
    context('Empty state', () => {
        let wrapper;

        const props = {
            weather: {
                status: null,
                isFetching: false,
                data: null,
                errors: null
            }
        };

        beforeEach(() => {
            wrapper = shallow(<HomeViewNotConnected {...props}/>);
        });

        it('should render correctly', () => {
            return expect(wrapper).to.be.ok;
        });

        it('should have the search input and the powered by link', () => {
            const searchInput = wrapper.find('input');
            expect(searchInput).to.have.length(1);
            expect(searchInput.prop('type')).to.equal('text');
            expect(wrapper.find('a')).to.have.length(1);
        });
    });

    context('Wrong search criteria', () => {
        let wrapper;
        const props = {
            weather: {
                status: 200,
                isFetching: false,
                data: null,
                errors: 'Wrong city name'
            }
        };

        beforeEach(() => {
            wrapper = shallow(<HomeViewNotConnected {...props}/>);
        });

        it('should render correctly', () => {
            return expect(wrapper).to.be.ok;
        });

        it('should have the search input, the powered by link and the error message', () => {
            expect(wrapper.find('input')).to.have.length(1);
            expect(wrapper.find('a')).to.have.length(1);
            const div = wrapper.find('div.home__search__errors p');
            expect(div).to.have.length(1);
            expect(div.text()).to.equal(props.weather.errors);
        });
    });

    describe('Store Integration', () => {
        context('State map', (done) => {
            nock('https://query.yahooapis.com')
                .get('/some/api')
                .reply(200, {
                    data: {
                        location: {
                            city: 'Porto'
                        },
                        item: {
                            condition: {
                                temp: '31',
                                text: 'Sunny'
                            },
                            forecast: [
                                { date: '11 Aug 2016', high: '20', low: '10', text: 'Sunny' },
                                { date: '12 Aug 2016', high: '35', low: '20', text: 'Sunny' },
                                { date: '13 Aug 2016', high: '31', low: '17', text: 'Sunny' },
                                { date: '14 Aug 2016', high: '31', low: '17', text: 'Sunny' },
                                { date: '15 Aug 2016', high: '31', low: '17', text: 'Sunny' }
                            ]
                        },
                        units: {
                            temperature: 'C'
                        }
                    }
                });

            const state = {
                weather: {
                    data: {
                        location: {
                            city: 'Porto'
                        },
                        item: {
                            condition: {
                                temp: '31',
                                text: 'Sunny'
                            },
                            forecast: [
                                { date: '11 Aug 2016', high: '20', low: '10', text: 'Sunny' },
                                { date: '12 Aug 2016', high: '35', low: '20', text: 'Sunny' },
                                { date: '13 Aug 2016', high: '31', low: '17', text: 'Sunny' },
                                { date: '14 Aug 2016', high: '31', low: '17', text: 'Sunny' },
                                { date: '15 Aug 2016', high: '31', low: '17', text: 'Sunny' }
                            ]
                        },
                        units: {
                            temperature: 'C'
                        }
                    }
                }
            };

            const expectedActions = [
                {
                    type: TYPES.WEATHER_REQUEST,
                    payload: {
                        isFetching: true
                    }
                }, {
                    type: TYPES.WEATHER_SUCCESS,
                    payload: {
                        isFetching: false,
                        status: null,
                        data: null,
                        errors: null
                    }
                },
                {
                    type: TYPES.WEATHER_FAILURE,
                    payload: {
                        isFetching: false,
                        status: null,
                        data: null,
                        errors: null
                    }
                }
            ];

            const middlewares = [thunk];
            const mockStore = configureStore(middlewares);
            const store = mockStore(state, expectedActions, done);

            const wrapper = mount(<HomeViewConnected store={store}/>);

            it('props', () => {
                expect(wrapper.node.renderedElement.props.weather.data).to.equal(state.weather.data);
            });

            it('actions', () => {
                expect(wrapper.node.renderedElement.props.weather.data).to.not.equal(null);
            });

            const weatherData = wrapper.node.renderedElement.props.weather.data;
            it('should have a div with the forecast for today', () => {
                const div = wrapper.find('div.home__today-forecast');
                expect(div).to.have.length(1);
                expect(div.find('h1')).to.have.length(1);
                expect(div.find('h1').text()).to.equal(weatherData.location.city);

                expect(div.find('h2')).to.have.length(1);
                const today = moment(new Date(weatherData.item.forecast[0].date));
                const weekday = today.format('dddd');
                const date = today.format('MMMM D');
                expect(div.find('h2').text()).to.equal(`${weekday}${date}`);

                expect(div.find('div.home__forecast__temperature.home__forecast__temperature--high')).to.have.length(1);
                expect(div.find('div.home__forecast__temperature').text()).to.equal(
                    `${weatherData.item.condition.temp}°${weatherData.units.temperature}`
                );

                expect(div.find('p')).to.have.length(1);
            });

            it('Should have a div with the forecast for the next four days', () => {
                const div = wrapper.find('div.home__days-forecast__day');
                expect(div).to.have.length(4);
            });

            context('Each day on the four days forecast, should have', () => {
                const div = wrapper.find('div.home__days-forecast__day');
                const day = div.at(0);
                it('day name', () => {
                    expect(day.find('h2')).to.have.length(1);
                });

                it('temperature text', () => {
                    expect(day.find('p')).to.have.length(1);
                });

                it('low and high temperatures', () => {
                    expect(day.find('div.home__forecast__temperature')).to.have.length(2);
                });
            });

            nock.cleanAll();
        });
    });
});
