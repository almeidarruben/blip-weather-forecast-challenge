# Blip Weather Forecast Challenge

## Retrieve code

* `$ git clone https://bitbucket.org/almeidarruben/blip-weather-forecast-challenge.git`
* `$ cd blip-weather-forecast-challenge`

## Installation

* `$ npm install`

## Running

### Development mode
Will run webpack with watch and compile code as it changes

* `$ npm run dev`

### Production mode

* `$ npm run prod`

### Run HTTP server

* `$ cd dist`
* `$ python -m SimpleHTTPServer`

## Testing

* `$ npm run coverage`

## Static analysis

To make sure the code respects all coding guidelines.

* `$ ./scripts/static_validate.sh`
