import { createReducer } from '../utils/store';
import { WEATHER_REQUEST, WEATHER_SUCCESS, WEATHER_FAILURE } from '../constants';

const initialState = {
    status: null,
    isFetching: false,
    data: null,
    errors: null
};

export default createReducer(initialState, {
    [WEATHER_REQUEST]: (state) => {
        return Object.assign({}, state, {
            isFetching: true
        });
    },
    [WEATHER_SUCCESS]: (state, payload) => {
        return Object.assign({}, state, {
            isFetching: false,
            status: payload.status,
            data: payload.data,
            errors: null
        });
    },
    [WEATHER_FAILURE]: (state, payload) => {
        return Object.assign({}, state, {
            isFetching: false,
            status: payload.status,
            data: null,
            errors: payload.errors
        });
    }
});
