import fetch from 'isomorphic-fetch';
import { checkHttpStatus, parseJSON } from '../utils/requests';
import { WEATHER_REQUEST, WEATHER_SUCCESS, WEATHER_FAILURE } from '../constants';

export function weatherRequest() {
    return {
        type: WEATHER_REQUEST
    };
}

export function weatherSuccess(data, status = 200) {
    return {
        type: WEATHER_SUCCESS,
        payload: {
            status,
            data
        }
    };
}

export function weatherFailure(errors, status = 400) {
    return {
        type: WEATHER_FAILURE,
        payload: {
            status,
            errors
        }
    };
}

export function fetchWeather(city) {
    const placesQuery = `select woeid from geo.places(1) where text="${city}"`;
    const query = `select * from weather.forecast where woeid in (${placesQuery}) and u='c'`;
    return (dispatch) => {
        dispatch(weatherRequest());
        return fetch(`https://query.yahooapis.com/v1/public/yql?q=${encodeURIComponent(query)}&format=json`)
            .then(checkHttpStatus)
            .then(parseJSON)
            .then(response => {
                if (response.query.count === 0) {
                    dispatch(weatherFailure('Wrong city name', 200));
                } else {
                    dispatch(weatherSuccess(response.query.results.channel));
                }
            })
            .catch(error => {
                dispatch(weatherFailure(error));
            });
    };
}
