import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { browserHistory, Router, Route, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from './store/configureStore';
import { App, HomeView } from './containers';

const target = document.getElementById('root');
const store = configureStore(window.__INITIAL_STATE__, browserHistory);
const history = syncHistoryWithStore(browserHistory, store);


const appProvider = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App} store={store}>
                <IndexRoute component={HomeView}/>
            </Route>
        </Router>
    </Provider>
);

ReactDOM.render(appProvider, target);
