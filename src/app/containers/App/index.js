import React from 'react';
import ReactDOM from 'react-dom';
import '../../styles/main.scss';
import DevTools from '../DevTools';

export default class App extends React.Component {

    static propTypes = {
        children: React.PropTypes.object.isRequired,
        route: React.PropTypes.object.isRequired
    };

    static childContextTypes = {
        dispatch: React.PropTypes.func
    };

    getChildContext() {
        return {
            dispatch: this.props.route.store.dispatch
        };
    }

    componentDidMount() {
        if (process.env.NODE_ENV === 'development') {
            this.node = document.createElement('div');
            this.node.className = 'ReactDevTools';
            document.body.appendChild(this.node);
            this.renderDevTools();
        }
    }

    componentWillUnmount() {
        if (process.env.NODE_ENV === 'development') {
            ReactDOM.unmountComponentAtNode(this.node);
            document.body.removeChild(this.node);
        }
    }

    renderDevTools = () => {
        ReactDOM.unstable_renderSubtreeIntoContainer(this, <DevTools/>, this.node);
    };

    render() {
        return (
            this.props.children
        );
    }
}
