import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import moment from 'moment';
import './style.scss';
import { debounce } from '../../utils/debounce';
import { fetchWeather } from '../../actions/weather';

const spinnerImage = require('./images/spinner.svg');

class HomeView extends React.Component {

    static contextTypes = {
        dispatch: React.PropTypes.func
    };

    static propTypes = {
        weather: React.PropTypes.object
    };

    constructor(props) {
        super(props);
        this.initialWeatherState = this.props.weather;
        this.state = {
            weather: this.props.weather
        };
    }

    componentWillReceiveProps(newProps) {
        this.setState({
            weather: newProps.weather
        });
    }

    handleSearch = () => {
        if (this.refs.searchInput.value) {
            this.context.dispatch(fetchWeather(this.refs.searchInput.value));
        } else {
            this.setState({
                weather: this.initialWeatherState
            });
        }
    };

    render() {
        let searchInputClassNames = classNames({
            'home__search__input': true,
            'home__search__input--has-results': this.state.weather.data
        });

        // Spinner
        let spinner = null;
        if (this.state.weather.ifFetching) {
            spinner = (
                <div className="home__search__spinner">
                    <img src={spinnerImage}
                         width={22}
                         height={22}
                         alt="spinner"
                    />
                    <p>Fetching weather information</p>
                </div>
            );
        }

        // Errors
        let errors = null;
        if (this.state.weather.errors) {
            errors = (
                <div className="home__search__errors">
                    <p>{this.state.weather.errors}</p>
                </div>
            );
        }

        // Weather data
        let todayForecast = null;
        const daysForecast = [];
        if (this.state.weather.data) {
            const weatherData = this.state.weather.data;

            // Create a new javascript date from the string, avoid moment fallback warning
            const today = moment(new Date(weatherData.item.forecast[0].date));
            const weekday = today.format('dddd');
            const date = today.format('MMMM D');
            const temperatureClassNames = classNames({
                'home__forecast__temperature': true,
                'home__forecast__temperature--low': weatherData.item.condition.temp <= 15,
                'home__forecast__temperature--medium': weatherData.item.condition.temp > 15
                && weatherData.item.condition.temp < 30,
                'home__forecast__temperature--high': weatherData.item.condition.temp >= 30
            });
            todayForecast = (
                <div className="home__today-forecast">
                    <h1>{weatherData.location.city}</h1>
                    <h2>{weekday}<br/>{date}</h2>
                    <div className={temperatureClassNames}>
                        {weatherData.item.condition.temp}&deg;{weatherData.units.temperature}
                    </div>
                    <p>{weatherData.item.condition.text}</p>
                </div>
            );

            // Forecast for the next 4 days
            const forecasts = weatherData.item.forecast.splice(1, 4);
            for (const forecast of forecasts) {
                const dayName = moment(new Date(forecast.date)).format('dddd');
                const dayLowTemperatureClassNames = classNames({
                    'home__forecast__temperature': true,
                    'home__forecast__temperature--low': forecast.low <= 15,
                    'home__forecast__temperature--medium': forecast.low > 15 && forecast.low < 30,
                    'home__forecast__temperature--high': forecast.low >= 30
                });
                const dayHighTemperatureClassNames = classNames({
                    'home__forecast__temperature': true,
                    'home__forecast__temperature--low': forecast.high <= 15,
                    'home__forecast__temperature--medium': forecast.high > 15 && forecast.high < 30,
                    'home__forecast__temperature--high': forecast.high >= 30
                });
                daysForecast.push(
                    <div className="home__days-forecast__day"
                         key={dayName}
                    >
                        <div className="home__days-forecast__day__info">
                            <h2>{dayName}</h2>
                            <p>{forecast.text}</p>
                        </div>
                        <div className="home__days-forecast__day__temperature">
                            <div className={dayLowTemperatureClassNames}>
                                {forecast.low}&deg;
                            </div>
                            <div className={dayHighTemperatureClassNames}>
                                {forecast.high}&deg;
                            </div>
                        </div>
                    </div>
                );
            }
        }
        return (
            <div className="home">
                <div className="home__search">
                    <input type="text"
                           className={searchInputClassNames}
                           placeholder="Type a city name"
                           onKeyUp={debounce(() => {
                               this.handleSearch();
                           }, 600)}
                           ref="searchInput"
                    />
                    <a href="https://www.yahoo.com/?ilc=401"
                       target="_blank"
                    >
                        <img src="https://poweredby.yahoo.com/purple.png"
                             width="134"
                             height="29"
                             alt="Powered by Yahoo"
                        />
                    </a>
                    {spinner}
                    {errors}
                </div>

                {todayForecast}

                <div className="home__days-forecast">
                    {daysForecast}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        weather: state.weather
    };
};

export default connect(mapStateToProps)(HomeView);
export { HomeView as HomeViewNotConnected };
