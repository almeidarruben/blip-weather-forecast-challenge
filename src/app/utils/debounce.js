export function debounce(fn, delay) {
    let timer = null;
    return (...args) => {
        const context = this;

        clearTimeout(timer);
        const timeFunction = () => {
            fn.apply(context, args);
        };

        timer = setTimeout(timeFunction, delay);
    };
}
